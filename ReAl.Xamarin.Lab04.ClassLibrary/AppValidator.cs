﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReAl.Xamarin.Lab04.ClassLibrary
{
    public class AppValidator
    {
        private IDialog miDialog;

        public AppValidator(IDialog platformDialog)
        {
            miDialog = platformDialog;
        }

        public string strEmail { get; set; }
        public string strPassword { get; set; }
        public string strDevice { get; set; }

        public async void validate()
        {
            string strResultado;

            var miServicio = new SALLab04.ServiceClient();
            var miResultado = miServicio.ValidateAsync(strEmail, strPassword, strDevice);

            strResultado = $"{miResultado.Status}\n{miResultado.Id}";

            miDialog.showMessage(strResultado);
        }
    }
}
