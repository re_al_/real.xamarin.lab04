﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ReAl.Xamarin.Lab04.Android
{
    class AndroidDialog : ClassLibrary.IDialog
    {
        private Context miContext;

        public AndroidDialog(Context appContext)
        {
            miContext = appContext;
        }

        public void showMessage(string strMensaje)
        {
            AlertDialog.Builder miBuilder = new AlertDialog.Builder(miContext);
            AlertDialog miAlert = miBuilder.Create();
            miAlert.SetTitle("Resultado de Verificación");
            miAlert.SetIcon(Resource.Drawable.Icon);
            miAlert.SetMessage(strMensaje);
            miAlert.SetButton("Ok", (sender, args) => {});
            miAlert.Show();
        }


    }
}