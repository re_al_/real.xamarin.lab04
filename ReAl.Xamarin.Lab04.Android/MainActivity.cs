﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Provider;
using ReAl.Xamarin.Lab04.ClassLibrary;

namespace ReAl.Xamarin.Lab04.Android
{
    [Activity(Label = "ReAl.Xamarin.Lab04.Android", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            // SetContentView (Resource.Layout.Main);

            var miProyecto = new AppValidator(new AndroidDialog(this));
            miProyecto.strEmail = "xxxx@gmail.com";
            miProyecto.strPassword = "PASSWORD";
            miProyecto.strDevice = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);

            miProyecto.validate();
        }
    }
}

