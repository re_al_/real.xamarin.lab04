﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReAl.Xamarin.Lab04.ClassLibrary;

namespace ReAl.Xamarin.Lab04.UWP
{
    class UWPDialog : IDialog
    {
        public async void showMessage(string strMensaje)
        {
            var miDialog = new Windows.UI.Popups.MessageDialog(strMensaje);
            await miDialog.ShowAsync();
        }
    }
}
